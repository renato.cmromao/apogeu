#!/bin/bash

echo "Vamos começar: [s/n]"
read CONT

iptables -I INPUT -p all -j DROP

echo " "

while [ "$CONT" = "s" ]
do

echo "---*** MENU ***---
 ESCOLHA UMA OPÇÃO: 
1 - Ver as regras de IPTABLES 
2 - Liberando uma porta para um IP específico
3 - Excluindo uma regra
4 - Redirecionar portas
5 - Ver regras de redirecionamento de portas
6 - Excluir regras de redirecionamento de portas 
7 - Sair do MENU"

read OPC

case $OPC in
	1) /regras.sh;;
	2) /desbloq_icmp.sh;;
	3) /bloq_icmp.sh;;
	4) /redirec_portas.sh;;
	5) /ver_redirec.sh;;
	6) /exc_redirec.sh;;
	7) echo "Saindo do MENU.."
	sleep 2 | exit;;       	
	*) echo "Opção inválida";;

esac
echo " "
echo "Deseja continuar? [s/n] "
read SAIR
echo " "
if [ "$SAIR" = "n" ]
then
	echo "Obrigado por usar o nosso programa!"
	sleep 2
	clear
	exit
else
	echo "Continuando..."
	sleep 2
	echo " "
fi

done
echo " "
echo "Obrigado!"
sleep 3
clear

